from sqlite3.dbapi2 import Cursor, IntegrityError

from utils import print_header, print_people


def create_tables(cursor: Cursor) -> None:
    cursor.execute("PRAGMA foreign_keys=ON;")

    # Creates table `position`
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS position (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            name VARCHAR(50) NOT NULL
        );
        """
    )
    # Inserts static data into `position` if table `position` is empty
    cursor.execute(
        """
        INSERT INTO position
        SELECT * FROM 
        (VALUES 
            (1, 'Composer'),
            (2, 'Fighter'),
            (3, 'Detective'),
            (4, 'Actor'),
            (5, 'Actress'),
            (6, 'U.B.C.S.'),
            (7, 'S.T.A.R.S.'), 
            (8, 'TerraSave'),
            (9, 'Dominant of the Phoenix'),
            (10, 'Cybersport'),
            (11, 'Cat breed'),
            (12, 'Kung fu'),
            (13, 'Project Manager'),
            (14, 'Web Designer'),
            (15, 'Software Engineer')
        )
        WHERE NOT EXISTS (SELECT * FROM position);
        """
    )

    # Creates table `gender`
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS gender (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            name VARCHAR(13) NOT NULL
        );
        """
    )
    # Inserts static data into `gender` if table `gender` is empty
    cursor.execute(
        """
        INSERT INTO gender
        SELECT * FROM 
        (VALUES 
            (1, "not set"),
            (2, "male"),
            (3, "female")
        )
        WHERE NOT EXISTS (SELECT * FROM gender);
        """
    )

    # Creates table `people`
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS people (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            first_name VARCHAR(20) NOT NULL,
            last_name VARCHAR(20) NOT NULL,
            gender INTEGER REFERENCES gender (id) 
            ON DELETE CASCADE 
            ON UPDATE CASCADE NOT NULL,
            salary INT NOT NULL CHECK (salary > 0),
            position INTEGER REFERENCES position (id) 
            ON DELETE SET NULL
            ON UPDATE CASCADE,
            email VARCHAR(70) UNIQUE,
            age INT NOT NULL CHECK (age > 0)
        );
        """
    )


def insert_twenty_people(cursor: Cursor) -> None:
    cursor.execute(
        """
        INSERT INTO people 
            (first_name,    last_name,   gender, salary, position, email,        age) 
        SELECT * FROM (
            VALUES
            ('Nobuo',       'Uematsu',   2,      20100,  1,        'nu@mac.com', 62 ),
            ('Leroy',       'Smith',     2,      19100,  2,        'ls@mac.com', 50 ),
            ('James',       'Carter',    2,      18100,  3,        'js@mac.com', 51 ),
            ('Robert',      'Downey',    2,      17100,  4,        'rd@mac.com', 56 ),
            ('Mikhail',     'Victor',    2,      16100,  6,        'mv@mac.com', 45 ),
            ('Joseph',      'Frost',     2,      15100,  7,        NULL,         27 ),
            ('Tom',         'Cruise',    2,      14100,  4,        NULL,         59 ),
            ('Christopher', 'Redfield',  2,      13100,  7,        NULL,         23 ),
            ('Claire',      'Redfield',  3,      12100,  8,        NULL,         21 ),
            ('Nina',        'Williams',  3,      11100,  2,        NULL,         21 ),
            ('Anna',        'Williams',  3,      10100,  NULL,     NULL,         21 ),
            ('Joshua',      'Rosfield',  2,      9100,   NULL,     NULL,         10 ),
            ('Edward',      'Navi',      2,      8100,   NULL,     NULL,         21 ),
            ('Scottish',    'Fold',      1,      7100,   NULL,     NULL,         60 ),
            ('Julia',       'Chang',     3,      6100,   NULL,     NULL,         21 ),
            ('Rachel',      'Dawes',     3,      5100,   NULL,     NULL,         30 ),
            ('Bruce',       'Wayne',     2,      4100,   NULL,     NULL,         31 ),
            ('Po',          'Ping',      2,      3100,   NULL,     NULL,         35 ),
            ('Lana',        'Wachowski', 3,      2100,   NULL,     NULL,         10 ),
            ('Crash',       'Bandicoot', 2,      1100,   NULL,     NULL,         11 )
        )
        WHERE NOT EXISTS (SELECT * FROM people);
        """
    )


def add_new_gender(cursor: Cursor) -> None:
    cursor.execute(
        """
        INSERT INTO gender
        SELECT * FROM
        (VALUES (4, 'not specified'))
        WHERE NOT EXISTS 
        (SELECT * FROM gender 
        WHERE gender.id = 4);
        """
    )


def add_person_with_non_existent_position(cursor: Cursor) -> None:
    try:
        cursor.execute(
            """
            INSERT INTO people 
            (first_name, last_name, gender, salary, position, email, age) VALUES
            ('Alien', 'Wachowski', 4, 21100, 909, NULL, 51);
            """
        )
    except IntegrityError:
        print_header("COULDN'T INSERT PERSON WITH NON-EXISTENT POSITION")


def random_person_update_new_gender(cursor: Cursor) -> None:
    cursor.execute(
        f"""
        UPDATE people 
        SET gender = 4
        WHERE id = (SELECT id FROM people
        ORDER BY RANDOM()
        LIMIT 1);
        """
    )


def print_people_with_new_gender(cursor: Cursor) -> None:
    people = cursor.execute(
        """
        SELECT * FROM people
        WHERE gender = 4;
        """
    )

    print_people(people, "PEOPLE WITH NEW GENDER (id=4)")


def delete_new_gender_and_print_people_deleted_gender(cursor: Cursor) -> None:
    cursor.execute(
        """
        DELETE FROM gender
        WHERE name = 'not specified';
        """
    )

    gender = 4
    people = cursor.execute(
        f"""
        SELECT * FROM people
        WHERE gender = {gender};
        """
    ).fetchall()
    if not people:
        print_header(f"Не найдено людей для указанного гендера {gender}")
        return

    print_people(people, "AFTER DELETION NEW GENDER (id=4)")


def people_join_FKs_print_with_salary_greater_tensouth_gender_position_notnull(
    cursor: Cursor,
) -> None:
    people = cursor.execute(
        """
        SELECT
            people.id,
            people.first_name,
            people.last_name,
            gender.name,
            people.salary,
            position.name,
            people.email,
            people.age
        FROM people
        JOIN gender ON people.gender = gender.id
        JOIN position ON people.position = position.id
        WHERE salary > 10000 
        AND people.gender IS NOT NULL
        AND people.position IS NOT NULL;
        """
    )

    print_people(
        people,
        "PEOPLE JOIN FKs WHERE SALARY GREATER THAN 10000 AND GENDER/POSITION NOT NULL",
    )


def people_left_join_FKs_print_with_salary_greater_tensouth_gender_notnull(
    cursor: Cursor,
) -> None:
    people = cursor.execute(
        """
        SELECT
            people.id,
            people.first_name,
            people.last_name,
            gender.name,
            people.salary,
            position.name,
            people.email,
            people.age
        FROM people
        LEFT JOIN gender ON people.gender = gender.id
        LEFT JOIN position ON people.position = position.id
        WHERE salary > 10000 AND people.gender IS NOT NULL;
        """
    )

    print_people(
        people,
        "PEOPLE LEFT JOIN FKs WHERE SALARY GREATER THAN 10000 AND GENDER NOT NULL",
    )


def delete_position_and_print_previous_people(cursor: Cursor) -> None:
    cursor.execute(
        """
        DELETE FROM position
        WHERE id = (SELECT id FROM position
        ORDER BY RANDOM()
        LIMIT 1);
        """
    )

    people = cursor.execute(
        """
        SELECT
            people.id,
            people.first_name,
            people.last_name,
            gender.name,
            people.salary,
            position.name,
            people.email,
            people.age
        FROM people
        LEFT JOIN gender ON people.gender = gender.id
        LEFT JOIN position ON people.position = position.id
        WHERE salary > 10000 AND people.gender IS NOT NULL;
        """
    )

    print_people(
        people,
        "AFTER DELETED POSITION (PREVIOUS TABLE)",
    )


def drop_tables(cursor: Cursor) -> None:
    cursor.execute("DROP TABLE IF EXISTS people;")
    cursor.execute("DROP TABLE IF EXISTS gender;")
    cursor.execute("DROP TABLE IF EXISTS position;")
