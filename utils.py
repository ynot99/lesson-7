import pandas
import inspect


def print_header(message: str) -> None:
    """
    Prints blue header
    """

    cur_frame = inspect.currentframe().f_back.f_lineno
    print(
        f"\n\033[96m---\t{message}\t---\treadable.py line {cur_frame}\033[0m\n"
    )


def print_people(people: list[tuple], message: str) -> None:
    """
    Prints people table
    Required dict keys: "id", "first_name", "last_name", "gender", "salary", "position", "email", "age"
    """

    # TODO remove copy/paste
    cur_frame = inspect.currentframe().f_back.f_lineno
    print(
        f"\n\033[96m---\t{message}\t---\treadable.py line {cur_frame}\033[0m\n"
    )
    print(
        pandas.DataFrame(
            people,
            columns=[
                "id",
                "first_name",
                "last_name",
                "gender",
                "salary",
                "position",
                "email",
                "age",
            ],
        )
    )
