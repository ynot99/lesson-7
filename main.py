import sqlite3
import sys

from readable import *

with sqlite3.connect("homework7.db") as conn:
    cursor = conn.cursor()

    if "drop" in sys.argv:
        drop_tables(cursor)

    # 1-5
    create_tables(cursor)

    # 6
    insert_twenty_people(cursor)

    # 7
    add_new_gender(cursor)

    # 8 (try/except inside to save beautiful code structure)
    add_person_with_non_existent_position(cursor)

    # 9
    random_person_update_new_gender(cursor)

    # 10
    print_people_with_new_gender(cursor)

    # 11
    delete_new_gender_and_print_people_deleted_gender(cursor)

    # 12 (FK - foreign key) don't blame me for long function name, please :(
    people_join_FKs_print_with_salary_greater_tensouth_gender_position_notnull(
        cursor
    )

    # 13
    people_left_join_FKs_print_with_salary_greater_tensouth_gender_notnull(
        cursor
    )

    # 14
    delete_position_and_print_previous_people(cursor)

    # 15
    conn.commit()

    cursor.close()
